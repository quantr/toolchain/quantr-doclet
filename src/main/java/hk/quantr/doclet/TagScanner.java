package hk.quantr.doclet;

import com.sun.source.doctree.AuthorTree;
import com.sun.source.doctree.DocCommentTree;
import com.sun.source.doctree.ParamTree;
import com.sun.source.doctree.ReturnTree;
import com.sun.source.doctree.UnknownBlockTagTree;
import com.sun.source.doctree.UnknownInlineTagTree;
import com.sun.source.util.SimpleDocTreeVisitor;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class TagScanner extends SimpleDocTreeVisitor<Void, Void> {

	private final Map<String, List<String>> tags;

	public TagScanner(Map<String, List<String>> tags) {
		this.tags = tags;
	}

	@Override
	public Void visitDocComment(DocCommentTree tree, Void p) {
		return visit(tree.getBlockTags(), null);
	}

	@Override
	public Void visitUnknownBlockTag(UnknownBlockTagTree tree, Void p) {
		String name = tree.getTagName();
		System.out.println("visitUnknownBlockTag=" + name);
		String content = tree.getContent().toString();
		tags.computeIfAbsent(name, n -> new ArrayList<>()).add(content);
		return null;
	}

	@Override
	public Void visitUnknownInlineTag(UnknownInlineTagTree tree, Void p) {
		String name = tree.getTagName();
		System.out.println("visitUnknownInlineTag=" + name);
		String content = tree.getContent().toString();
		tags.computeIfAbsent(name, n -> new ArrayList<>()).add(content);
		return null;
	}

	@Override
	public Void visitParam(ParamTree tree, Void p) {
		String name = tree.getTagName() + " " + tree.getName();

		String content = tree.getDescription().stream().map(ri -> ri.toString()).collect(Collectors.joining(","));
		System.out.println("visitParam=" + name + " = " + content);
		tags.computeIfAbsent(name, n -> new ArrayList<>()).add(content);
		return null;
	}

	@Override
	public Void visitAuthor(AuthorTree node, Void p) {
		String content = node.toString().replaceFirst("@author *", "");
		System.out.println("visitAuthor=" + content);
		tags.computeIfAbsent("author", n -> new ArrayList<>()).add(content);
		return null;
	}

	@Override
	public Void visitReturn(ReturnTree node, Void p) {
		String content = node.toString().replaceFirst("@return *", "");
		System.out.println("visitReturn=" + content);
		tags.computeIfAbsent("return", n -> new ArrayList<>()).add(content);
		return null;
	}

}

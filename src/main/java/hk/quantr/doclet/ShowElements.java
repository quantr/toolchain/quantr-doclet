package hk.quantr.doclet;

import com.sun.source.doctree.DocCommentTree;
import com.sun.source.util.DocTrees;
import com.sun.source.util.TreePath;
import hk.quantr.doclet.structure.JavaClass;
import hk.quantr.doclet.structure.JavaMethod;
import hk.quantr.doclet.structure.JavaPackage;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.stream.Collectors;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.ModuleElement;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.VariableElement;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.util.ElementScanner9;
import javax.lang.model.util.Types;
import javax.tools.JavaFileObject;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class ShowElements extends ElementScanner9<Void, Integer> {

	final PrintWriter out;
	DocTrees treeUtils;
	Types typeUtils;
	ArrayList<JavaPackage> data;

	public ShowElements(PrintWriter out, DocTrees treeUtils, Types typeUtils, ArrayList<JavaPackage> data) {
		this.out = out;
		this.treeUtils = treeUtils;
		this.typeUtils = typeUtils;
		this.data = data;
	}

	void show(Set<? extends Element> elements) {
		scan(elements, 0);
	}

	public HashMap<String, String> tagsToString(DocCommentTree dcTree) {
		Map<String, List<String>> tags = new TreeMap<>();
		HashMap<String, String> r = new HashMap<>();
		new TagScanner(tags).visit(dcTree, null);
		tags.forEach((t, l) -> {
//				l.forEach(c -> out.println(" = " + c));
			String tagValue = l.stream().collect(Collectors.joining(","));
			r.put(t, tagValue);
		});
		return r;
	}

	@Override
	public Void scan(Element e, Integer depth) {
		Void temp = super.scan(e, depth + 1);
		DocCommentTree dcTree = treeUtils.getDocCommentTree(e);
		if (dcTree != null) {
			String indent = "  ".repeat(depth);
			if (dcTree.getFullBody().size() > 0) {
				JavaPackage lastPackage = data.get(data.size() - 1);
				JavaClass lastClass = lastPackage.classes.get(lastPackage.classes.size() - 1);

				lastClass.comment = dcTree.getFullBody().toString();
				lastClass.tags = tagsToString(dcTree);
			}

		}
		return temp;
	}

	@Override
	public Void visitModule(ModuleElement e, Integer p) {
		out.println("visitModule=" + e);
		return super.visitModule(e, p);
	}

	@Override
	public Void visitExecutable(ExecutableElement ee, Integer depth) {
		String indent = "  ".repeat(depth);

		out.println("visitExecutable=" + indent + ee.getKind() + ": " + ee.getSimpleName() + " > " + ee);
		if (!ee.getTypeParameters().isEmpty()) {
			out.println(indent + "[Type Parameters]");
			scan(ee.getTypeParameters(), depth);
		}
		if (ee.getKind() == ElementKind.METHOD) {
			JavaPackage lastPackage = data.get(data.size() - 1);
			JavaClass lastClass = lastPackage.classes.get(lastPackage.classes.size() - 1);

			DocCommentTree dcTree = treeUtils.getDocCommentTree(ee);
			JavaMethod javaMethod = new JavaMethod(ee.getSimpleName().toString(), ee.getReturnType() + " " + ee, dcTree == null ? null : dcTree.getFullBody().toString());
			javaMethod.tags = tagsToString(dcTree);
			lastClass.methods.add(javaMethod);
			System.out.println("\tfull: " + ee.getReturnType() + " " + ee);
			show("Return type", ee.getReturnType(), depth);
		}
		if (!ee.getParameters().isEmpty()) {
			out.println(indent + "[Parameters]");
			scan(ee.getParameters(), depth);
		}
		show("Throws", ee.getThrownTypes(), depth);

		return super.visitExecutable(ee, depth);
	}

	@Override
	public Void visitVariable(VariableElement ve, Integer depth) {
		if (ve.getKind() == ElementKind.PARAMETER) {
			String indent = "visitVariable=" + "  ".repeat(depth);
			out.println(indent + ve.getKind() + ": " + ve);
			show("Type", ve.asType(), depth);
		}
		return super.visitVariable(ve, depth);
	}

	private void show(String label, List<? extends TypeMirror> list, int depth) {
		if (!list.isEmpty()) {
			String indent = "  ".repeat(depth);
			out.println(indent + "[" + label + "]");
			int i = 0;
			for (TypeMirror tm : list) {
				show("#" + (i++), tm, depth + 1);
			}
		}
	}

	private void show(String label, TypeMirror tm, int depth) {
		String indent = "  ".repeat(depth);
		out.println(indent + "[" + label + "]");
		out.println(indent + "  TypeMirror: " + tm);
		out.println(indent + "  as Element: " + typeUtils.asElement(tm));
	}

	@Override
	public Void visitType(TypeElement te, Integer p) {
		TreePath dct = treeUtils.getPath(te);
		if (dct == null) {
			out.println(te + ": no source file found");
		} else {
			JavaFileObject fo = dct.getCompilationUnit().getSourceFile();

			out.println("+".repeat(100));
			out.println("visitType: packageName=" + dct.getCompilationUnit().getPackageName() + ", " + te + ": " + fo.getName());
			data.add(new JavaPackage(dct.getCompilationUnit().getPackageName().toString()));
			data.get(data.size() - 1).classes.add(new JavaClass(te.getSimpleName().toString()));
		}
		return super.visitType(te, p);
	}
}

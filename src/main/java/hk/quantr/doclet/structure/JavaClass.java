package hk.quantr.doclet.structure;

import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class JavaClass {

	public String name;
	public String comment;
	public HashMap<String, String> tags = new HashMap<>();
	public ArrayList<JavaMethod> methods = new ArrayList<>();

	public JavaClass(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		String temp = "";
		for (var a : methods) {
			temp += "\t\t" + a + "\n";
		}
		return "JavaClass{" + "name=" + name + ", comment=" + comment + "}\n" + temp;
	}

	public String toHTML() {
		String r = "<div class=\"row\">\n"
				+ "	<div class=\"col-3\">Class</div>\n"
				+ "	<div class=\"col\">" + name + "</div>\n"
				+ "</div>\n";
		if (comment != null) {
			r += "<div class=\"row\">\n"
					+ "	<div class=\"col\">Comment</div>\n"
					+ "</div>\n"
					+ "<div class=\"row\">\n"
					+ "	<div class=\"col\">\n"
					+ "		<pre>" + comment + "</pre>\n"
					+ "	</div>\n"
					+ "	</div>\n";
		}
		String temp = "";
		for (var a : methods) {
			temp += a.toHTML();
		}
		r += temp;
		return r;
	}

}

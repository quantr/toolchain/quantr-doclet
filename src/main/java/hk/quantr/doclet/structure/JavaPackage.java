package hk.quantr.doclet.structure;

import java.util.ArrayList;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class JavaPackage {

	String name;
	public ArrayList<JavaClass> classes = new ArrayList<>();

	public JavaPackage(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		String temp = "";
		for (var c : classes) {
			temp += "\t" + c;
		}
		return "JavaPackage{" + "name=" + name + "\n" + temp + '}';
	}

	public String toHTML() {
		String temp = "";
		for (var c : classes) {
			temp += c.toHTML();
		}
		return "<div class=\"row\">\n"
				+ "	<div class=\"col-3\">Package</div>\n"
				+ "	<div class=\"col\">" + name + "</div>\n"
				+ "</div>\n" + temp;
	}

}

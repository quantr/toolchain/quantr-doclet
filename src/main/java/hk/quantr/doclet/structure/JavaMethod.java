package hk.quantr.doclet.structure;

import java.util.HashMap;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class JavaMethod {

	public String name;
	public String signature;
	public String comment;
	public HashMap<String, String> tags = new HashMap<>();

	public JavaMethod(String name, String signature, String comment) {
		this.name = name;
		this.signature = signature;
		this.comment = comment;
	}

	@Override
	public String toString() {
		return "JavaMethod{" + "name=" + name + ", signature=" + signature + ", comment=" + comment + '}';
	}

	public String toHTML() {
		String r = "<div class=\"row\">\n"
				+ "	<div class=\"col-3\">Method</div>\n"
				+ "	<div class=\"col\">" + signature + "</div>\n"
				+ "</div>\n";
		if (comment != null) {
			r += "<div class=\"row\">\n"
					+ "	<div class=\"col\">\n"
					+ "		<pre>" + comment + "</pre>\n"
					+ "	</div>\n"
					+ "</div>\n";
		}
		return r;
	}

}

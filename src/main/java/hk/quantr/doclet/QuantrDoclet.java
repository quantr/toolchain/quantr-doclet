package hk.quantr.doclet;

import com.sun.source.util.DocTrees;
import hk.quantr.doclet.structure.JavaPackage;
import hk.quantr.javalib.CommonLib;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.lang.model.SourceVersion;
import javax.lang.model.util.Types;
import jdk.javadoc.doclet.Doclet;
import jdk.javadoc.doclet.DocletEnvironment;
import jdk.javadoc.doclet.Reporter;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.text.StringSubstitutor;

/**
 * This doclet is to generate javadoc from maven project and put it on https://www.quantr.foundation
 *
 * @author Peter Cheung
 */
public class QuantrDoclet implements Doclet {

	Reporter reporter;
	DocTrees treeUtils;
	Types typeUtils;
	ArrayList<JavaPackage> data = new ArrayList<>();

	@Override
	public void init(Locale locale, Reporter reporter) {
		this.reporter = reporter;
	}

	@Override
	public String getName() {
		return "Quantr Doclet";
	}

	@Override
	public Set<? extends Doclet.Option> getSupportedOptions() {
		return Collections.emptySet();
	}

	@Override
	public SourceVersion getSupportedSourceVersion() {
		return SourceVersion.latest();
	}

	@Override
	public boolean run(DocletEnvironment environment) {
//		System.out.println("reporter: " + reporter);
//		environment.getSpecifiedElements().forEach(e -> reporter.print(Diagnostic.Kind.NOTE, e, e.getKind().toString()));
//		System.out.println("-".repeat(100));
//		environment.getSpecifiedElements().forEach(e -> System.out.println(">>>" + e.getKind().toString() + "\t" + e));
//		System.out.println("-".repeat(100));

		treeUtils = environment.getDocTrees();
		typeUtils = environment.getTypeUtils();

		new ShowElements(reporter.getStandardWriter(), treeUtils, typeUtils, data).show(environment.getSpecifiedElements());
		reporter.getStandardWriter().flush();
		System.out.println("-".repeat(100));

		dump();
		System.out.println("-".repeat(100));
		dumpHTML();

		System.out.println(CommonLib.objectToJson(data));

		data.add(0, new JavaPackage(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date())));
		try {
			FileUtils.writeStringToFile(new File("data.json"), CommonLib.objectToJson(data), "utf8");
		} catch (IOException ex) {
			Logger.getLogger(QuantrDoclet.class.getName()).log(Level.SEVERE, null, ex);
		}
		return true;
	}

	void dump() {
		for (var a : data) {
			System.out.println(a);
		}
	}

	void dumpHTML() {
		String html = "";
		for (var a : data) {
			html += a.toHTML() + "\b\n";
		}
		try {
			String templateString = IOUtils.toString(QuantrDoclet.class.getClassLoader().getResourceAsStream("hk/quantr/doclet/template1.html"), "utf8");
			Map<String, String> valuesMap = new HashMap();
			valuesMap.put("mainBody", html);
			StringSubstitutor sub = new StringSubstitutor(valuesMap);
			String resolvedString = sub.replace(templateString);

			FileUtils.writeStringToFile(new File("index.html"), resolvedString, "utf8");
		} catch (IOException ex) {
			Logger.getLogger(QuantrDoclet.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

}

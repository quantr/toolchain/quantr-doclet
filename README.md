# Quantr Doclet

Javadoc doclet to build doc and export html to our website https://www.quantr.foundation

![](https://www.quantr.foundation/wp-content/uploads/2023/03/Quantr-Doclet.drawio.svg)

# How to use

## By maven

in any maven project

```
	<build>
		<plugins>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-javadoc-plugin</artifactId>
				<version>3.4.1</version>
				<configuration>
					<stylesheetfile>${basedir}/src/main/javadoc/stylesheet.css</stylesheetfile>
					<show>public</show>
					<doclet>hk.quantr.doclet.QuantrDoclet</doclet>
					<docletArtifact>
						<groupId>hk.quantr</groupId>
						<artifactId>quantr-doclet</artifactId>
						<version>1.0</version>
					</docletArtifact>
					<useStandardDocletOptions>false</useStandardDocletOptions>
				</configuration>
			</plugin>
		</plugins>
	</build>
```

```
mvn javadoc:javadoc
```

## By command line

```
C:\workspace\quantr-doclet>javadoc -cp target\quantr-doclet-1.0-jar-with-dependencies.jar -doclet hk.quantr.doclet.QuantrDoclet -docletpath target\quantr-doclet-1.0-jar-with-dependencies.jar -sourcepath src/main/java hk.quantr.doclet
```

the "-cp" is for run time finding the libraries we have used in quantr-doclet, the "-docletpath" telling javadoc where to find our QuantrDoclet class. They are two different things. After succesfully ran, you will got data.json and index.html in current folder.



